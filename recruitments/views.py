from copy import deepcopy
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from sklearn.cluster import KMeans
import json

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views import generic, View
from django.urls import reverse_lazy, reverse
from django.utils import timezone
from .models import Recruitment, Question, RecruitmentAttempt, RecruitmentAttemptResponse


class CreateRecruitment(generic.CreateView, LoginRequiredMixin):
    model = Recruitment
    fields = ('name', 'description', 'number_of_questions', 'correct_answer_score',
              'incorrect_answer_score', 'expires_on', 'question')
    template_name = 'recruitments/create-recruitment.html'
    success_url = reverse_lazy('recruitments:recruitment_list')


class RecruitmentList(generic.ListView, LoginRequiredMixin):
    model = Recruitment
    template_name = 'recruitments/recruitment-list.html'
    context_object_name = 'recruitments'

    def get_context_data(self, **kwargs):
        """ Insert the single object into the context dict. """

        context = super(generic.ListView, self).get_context_data(**kwargs)
        context['rec_attempt'] = [rec.recruitment for rec in
                                  RecruitmentAttempt.objects.filter(user=self.request.user)]
        return context


class CreateQuestion(generic.CreateView, LoginRequiredMixin):
    model = Question
    template_name = 'recruitments/create-question.html'
    fields = '__all__'
    success_url = reverse_lazy('recruitments:question_list')


class QuestionList(generic.ListView, LoginRequiredMixin):
    model = Question
    template_name = 'recruitments/question-list.html'
    context_object_name = 'questions'

    def get_context_data(self, **kwargs):
        """ Insert the single object into the context dict. """

        context = super(generic.ListView, self).get_context_data(**kwargs)
        context['qstn_list'] = Question.objects.filter().exclude(subject='eq')
        return context


class StartTest(generic.View, LoginRequiredMixin):
    def get(self, request):
        recruitment = Recruitment.objects.get(id=request.GET['id'])
        question_list = recruitment.question.all()
        question_ids = [question.id for question in question_list]
        recruitment_attempt = RecruitmentAttempt.objects.create(
            user=request.user,
            date=timezone.now().date(),
            recruitment=recruitment,
        )

        request.session['recruitment'] = recruitment.id
        request.session['recruitment_attempt'] = recruitment_attempt.id
        request.session['question_ids'] = question_ids
        request.session['current_index'] = 0

        return redirect('recruitments:answer_question')


class AnswerQuestion(generic.View, LoginRequiredMixin):
    def get(self, request):
        recruitment = Recruitment.objects.get(id=request.session['recruitment'])
        if request.session['current_index'] >= recruitment.number_of_questions:
            return redirect('recruitments:finish_test')

        question = Question.objects.get(id=request.session['question_ids'][request.session['current_index']])
        return render(request, 'recruitments/answer-question.html', {
            'question': question,
            'current_index': request.session['current_index'],
        })

    def post(self, request):
        question = Question.objects.get(id=request.session['question_ids'][request.session['current_index']])
        recruitment_attempt = RecruitmentAttempt.objects.get(id=request.session['recruitment_attempt'])
        answer = request.POST.get('answer', '')

        RecruitmentAttemptResponse.objects.create(
            recruitment_attempt=recruitment_attempt,
            question=question,
            response=answer,
            is_correct=answer == question.correct_answer,
        )

        request.session['current_index'] += 1
        return redirect('recruitments:answer_question')


class FinishTest(generic.View, LoginRequiredMixin):
    def get(self, request):
        recruitment_attempt = RecruitmentAttempt.objects.get(id=request.session['recruitment_attempt'])

        return render(request, 'recruitments/finish-test.html', {'recruitment_attempt': recruitment_attempt})


class Results(generic.ListView, LoginRequiredMixin):
    context_object_name = 'attempts'
    template_name = 'recruitments/results.html'

    def get_queryset(self):
        return RecruitmentAttempt.objects.filter(recruitment__id=self.request.GET['id'])


class Result(generic.ListView, LoginRequiredMixin):
    context_object_name = 'attempt'
    template_name = 'recruitments/result.html'

    def get_queryset(self):
        return RecruitmentAttempt.objects.get(recruitment__id=self.request.GET['id'],
                                              user=self.request.user)


# class AnalyseResult(View, LoginRequiredMixin):
#
#     def get(self, request):
#         results = RecruitmentAttempt.objects.filter(recruitment__id=self.request.GET['id'])
#         f1 = [data.subject_score[1][0] for data in results]
#         f2 = [data.subject_score[1][1] for data in results]
#         print(f1, f2)
#         marks_array = np.array(list(zip(f1, f2)))
#         kmeans = KMeans(n_clusters=3)
#         # Fitting the input data
#         kmeans = kmeans.fit(marks_array)
#         # Getting the cluster labels
#         labels = kmeans.predict(marks_array)
#         # Centroid values
#         centroid = kmeans.cluster_centers_
#         colors = ["g.", "r.", "c.", "y."]
#         for i in range(len(marks_array)):
#             plt.plot(marks_array[i][0], marks_array[i][1], colors[labels[i]], markersize=10)
#         plt.scatter(centroid[:, 0], centroid[:, 1], marker="*", s=150, linewidths=5, zorder=10)
#         plt.show()
#         return redirect('recruitments:recruitment_list')

class AnalyseResult(View, LoginRequiredMixin):

    def get(self, request):
        results = RecruitmentAttempt.objects.filter(recruitment__id=self.request.GET['id'])
        f2 = [data.score for data in results]
        f1 = [data.eq_score for data in results]
        if len(f2) < 4:
            return render(request, 'recruitments/warning.html')
        marks_array = np.array(list(zip(f2, f1)))
        kmeans = KMeans(n_clusters=3)
        # Fitting the input data
        kmeans = kmeans.fit(marks_array)
        # Getting the cluster labels
        labels = kmeans.predict(marks_array)
        # Centroid values
        centroid = kmeans.cluster_centers_
        print(centroid)

        group_1 = []
        group_2 = []
        group_3 = []

        colors = ["g.", "r.", "c.", "y."]
        for i in range(len(marks_array)):
            # plt.plot(marks_array[i][0], marks_array[i][1], colors[labels[i]], markersize=10)
            if labels[i] == 0:
                group_1.append(results[i])
            elif labels[i] == 1:
                group_2.append(results[i])
            else:
                group_3.append(results[i])
        avg_1 = sum([data.score for data in group_1])/len(group_1)
        avg_2 = sum([data.score for data in group_2]) / len(group_2)
        avg_3 = sum([data.score for data in group_3]) / len(group_3)

        if (avg_1 > avg_2) and (avg_1 > avg_3):
            top = group_1
        elif (avg_2 > avg_1) and (avg_2 > avg_3):
            top = group_2
        else:
            top = group_3

        if (avg_1 < avg_2) and (avg_1 < avg_3):
            below_average = group_1
        elif (avg_2 < avg_1) and (avg_2 < avg_3):
            below_average = group_2
        else:
            below_average = group_3

        if avg_1 < avg_2:
            if avg_2 < avg_3:
                average = group_2
            elif avg_3 < avg_1:
                average = group_1
            else:
                average = group_3
        else:
            if avg_1 < avg_3:
                average = group_1
            elif avg_2 < avg_3:
                average = group_2
            else:
                average = group_3
        data = {
            'top': top,
            'average': average,
            'below_average': below_average
        }
        # plt.scatter(centroid[:, 0], centroid[:, 1], marker="*", s=150, linewidths=5, zorder=10)
        # plt.savefig("media/test.svg")
        return render(request, 'recruitments/analyse.html', data)


class DisplayGraph(View):

    def get(self, request):
        return render(request, 'recruitments/displayResult.html')


class AnalyzeResult(View, LoginRequiredMixin):

    def get(self, request):
        results = RecruitmentAttempt.objects.filter(recruitment__id=self.request.GET['id'])
        f1 = [data.subject_score[1][0] for data in results]
        f2 = [data.subject_score[1][1] for data in results]
        marks_array = np.array(list(zip(f1, f2)))
        plt.scatter(f1, f2, c='black', s=7)

        # Euclidean Distance Caculator
        def dist(a, b, ax=1):
            return np.linalg.norm(a - b, axis=ax)

        k = 3
        # X coordinates of random centroids
        c_x = np.random.randint(0, np.max(marks_array) - 5, size=k)

        # Y coordinates of random centroids
        c_y = np.random.randint(0, np.max(marks_array) - 5, size=k)

        centroid = np.array(list(zip(c_x, c_y)), dtype=np.float32)

        c_old = np.zeros(centroid.shape)
        # Cluster Lables(0, 1, 2)
        clusters = np.zeros(len(marks_array))
        # Error func. - Distance between new centroids and old centroids
        error = dist(centroid, c_old, None)

        # Loop will run till the error becomes zero
        while error != 0:

            # Assigning each value to its closest cluster
            for i in range(len(marks_array)):
                distances = dist(marks_array[i], centroid)
                cluster = np.argmin(distances)
                clusters[i] = cluster
            # Storing the old centroid values
            c_old = deepcopy(centroid)
            # Finding the new centroids by taking the average value
            for i in range(k):
                points = [marks_array[j] for j in range(len(marks_array)) if clusters[j] == i]
                centroid[i] = np.mean(points, axis=0)
            error = dist(centroid, c_old, None)

        return redirect('recruitments:recruitment_list')

from django import forms


class RegisterForm(forms.Form):
    YEAR_CHOICES = [year for year in range(1985, 2019)]

    first_name = forms.CharField(max_length=20, widget=forms.TextInput(
        attrs={'placeholder': 'First name', 'class': 'form-control', 'required': 'required'}))
    last_name = forms.CharField(max_length=20, required=False, widget=forms.TextInput(
        attrs={'placeholder': 'Last name (optional)', 'class': 'form-control'}))

    # qualification = forms.CharField(max_length=20, widget=forms.TextInput(
    #     attrs={'placeholder': 'Educational Qualification', 'class': 'form-control', 'required': 'required'}))
    #
    # experience = forms.CharField(max_length=100, widget=forms.TextInput(
    #     attrs={'placeholder': 'Professional Experience', 'class': 'form-control', 'required': False}))

    phone = forms.CharField(max_length=20, widget=forms.TextInput(
        attrs={'placeholder': 'Phone Number ', 'class': 'form-control', 'required': False}))

    resume = forms.FileField()

    # date_of_birth = forms.DateField(widget=forms.SelectDateWidget(years=YEAR_CHOICES))
    # profile_picture = models.ImageField(blank=True, null=True, upload_to='accounts/users/profile-pictures')

    email = forms.EmailField(widget=forms.EmailInput(attrs={'placeholder': 'Email', 'class': 'form-control'}))
    password = forms.CharField(max_length=24, min_length=6, widget=forms.PasswordInput(
        attrs={'placeholder': 'Password', 'class': 'form-control'}))
    confirm_password = forms.CharField(max_length=24, min_length=6, widget=forms.PasswordInput(
        attrs={'placeholder': 'Confirm password', 'class': 'form-control'}))

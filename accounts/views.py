from django.shortcuts import render, redirect
from django.views import View
from django.contrib import messages
from django.db import IntegrityError
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import User
from .forms import RegisterForm


class RegisterView(View):
    def get(self, request):
        return render(request, 'accounts/register.html', {'form': RegisterForm()})

    def post(self, request):
        form = RegisterForm(request.POST, request.FILES)
        if form.is_valid():
            if form.cleaned_data['password'] != form.cleaned_data['confirm_password']:
                messages.add_message(request, messages.ERROR, 'Passwords do not match. Please check.')
                return render(request, 'accounts/register.html', {'form': form})

            try:
                # first_name = form.cleaned_data['first_name']
                # email = form.cleaned_data['email']
                # password = form.cleaned_data['password']
                del form.cleaned_data['confirm_password']
                # del form.cleaned_data['first_name']
                # del form.cleaned_data['email']
                # del form.cleaned_data['password']
                User.objects.create_user(**form.cleaned_data)
                messages.add_message(request, messages.SUCCESS, 'Account created. Please login to continue')
                return redirect('login')
            except IntegrityError:
                messages.add_message(request, messages.ERROR, 'This email is already registered')
                return render(request, 'accounts/register.html', {'form': form})
        print(form.errors)
        return render(request, 'accounts/register.html', {'form': form})


class HomeView(LoginRequiredMixin, View):

    def get(self, request):
        return render(request, 'accounts/home.html')


class UserDetailView(LoginRequiredMixin, View):

    def get(self, request):

        user = User.objects.get(id=request.GET['id'])
        return render(request, 'accounts/detail.html', {'candidate': user})

    def post(self, request):

        file = request.FILES['resume']
        if file.name.endswith('.pdf') or file.name.endswith('.doc'):
            request.user.resume = file
            request.user.save()
            return render(request, 'accounts/detail.html', {'user': request.user})
        return render(request, 'accounts/detail.html',
                      {'user': request.user, 'message': 'Enter a pdf or doc file'})


class IndexView(LoginRequiredMixin, View):

    def get(self, request):
        return redirect('login')


class LogoutView(LoginRequiredMixin, View):

    def get(self, request):
        return redirect('login')

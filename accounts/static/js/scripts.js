
jQuery(document).ready(function() {
	
    /*
        Fullscreen background
    */
    $.backstretch("assets/img/backgrounds/1.jpg");
    
    $('#top-navbar-1').on('shown.bs.collapse', function(){
    	$.backstretch("resize");
    });
    $('#top-navbar-1').on('hidden.bs.collapse', function(){
    	$.backstretch("resize");
    });
    
    /*
        Form
    */
    $('.registration-form fieldset:first-child').fadeIn('slow');
    $('.registration-form fieldset:first-child').addClass('active')
    
    $('.registration-form input[type="text"], .registration-form input[type="password"], .registration-form textarea').on('focus', function() {
    	$(this).removeClass('input-error');
    });
    
    // next step
    $('.registration-form .btn-next').on('click', function() {
        var parent_fieldset1 = $('.registration-form fieldset.step1');
        var parent_fieldset2 = $('.registration-form fieldset.step2');
        var parent_fieldset3 = $('.registration-form fieldset.step3');
        if(parent_fieldset1.hasClass('active'))
        {
                parent_fieldset1.find('input[type="text"], input[type="password"], textarea').each(function() {
                     if(($('#id_registration_number').val() == "" ) || ($('#id_organisation_name').val() == "")){
                         $(this).addClass('input-error');

                     }
                     else
                     {
                        parent_fieldset1.fadeOut(400, function() {
                            $(this).removeClass('active')
                        $(this).next().addClass("active").fadeIn();


                        if($('li.tab1').hasClass('active'))
                            {
                                $('li.tab2').addClass("active");
                                $('li.tab1').removeClass('active')
                            }
                     // $('.signtab-container li.active').removeClass("active");

                         });
                     }
                 });
            
        } 
        else if(parent_fieldset2.hasClass('active'))
        {
                parent_fieldset2.find('input[type="text"], input[type="number"], textarea').each(function() {
                    var zip_text_value = $('#id_zip_code').val();
                    var phone_text_value = $('#id_phone').val();
                     if((($('#id_name').val() == "" ) || ($('#id_address_line_1').val() == "")|| ($('#id_address_line_2').val() == "")|| ($('#id_city').val() == "")|| ($('#id_state').val() == "")|| ($('#id_zip_code').val() == "")|| ($('#id_phone').val() == ""))||((zip_text_value.length != 6) || (phone_text_value.length != 10))) {
                         $(this).addClass('input-error');
                     }
                     else
                     {
                        parent_fieldset2.fadeOut(400, function() {
                        $(this).next().addClass("active").fadeIn();

                        if($('li.tab2').hasClass('active'))
                            {
                                $('li.tab3').addClass("active");
                                $('li.tab2').removeClass('active')
                            }
                     // $('.signtab-container li.active').removeClass("active");

                         });
                     }
                 });
            
        } 
    	// var parent_fieldset = $(this).parents('fieldset');
     //    var num_fieldset = $('fieldset.numbers').hasClass("active");
     //    var zip = $('fieldset.numbers.active #id_zip_code').val();
     //    var phone = $("fieldset.numbers.active #id_phone").val();
     //    var parent_tab = $('.signtab-container li');
    	// var next_step = true;
    	
    	// parent_fieldset.find('input[type="text"], input[type="password"], textarea').each(function() {
    	// 	if($(this).val() == "") {
    	// 		$(this).addClass('input-error');
    	// 		next_step = false;
                
                
     //            // parent_tab.addClass("active");

    	// 	}
    	// 	else if((zip.length != 6)&&(phone.length != 10)) 
     //            {
    			
     //             $(this).addClass('input-error');
     //            next_step = false;
     //            // $('.signtab-container li.active').removeClass("active");
     //            // if($('li.tab1').hasClass('active'))
     //            // {
     //            //     $('li.tab2').addClass("active");
     //            //     $('li.tab1').removeClass('active')
     //            // }
     //            // else if($('li.tab2').hasClass('active'))
     //            // {
     //            //     $('li.tab3').addClass("active");
     //            //     $('li.tab2').removeClass('active')
     //            // }
    	// 	}
     //        else
     //        {
     //           $(this).removeClass('input-error');
     //        }
    	// });
    	
    	// if( next_step ) {
    	// 	parent_fieldset.fadeOut(400, function() {
	    // 		$(this).next().addClass("active").fadeIn();

     //             // $('.signtab-container li.active').removeClass("active");

	    // 	});
     //            if($('li.tab1').hasClass('active'))
     //            {
     //                $('li.tab2').addClass("active");
     //                $('li.tab1').removeClass('active')
     //            }
     //            else if($('li.tab2').hasClass('active'))
     //            {
     //                $('li.tab3').addClass("active");
     //                $('li.tab2').removeClass('active')
     //            }
    	// }
    	
    });
    
    // previous step
    $('.registration-form .btn-previous').on('click', function() {
    	$(this).parents('fieldset').fadeOut(400, function() {
            $(this).removeClass('active')
    		$(this).prev().fadeIn().addClass('active');
            active_tab = $('.signtab-container li.active')
              if(active_tab.hasClass('active'))
                {
                    active_tab.prev().addClass("active");
                    active_tab.removeClass('active')
                }
    	});
    });
    
    // submit
    $('.registration-form').on('submit', function(e) {
    	
    	$(this).find('input[type="text"], input[type="password"], textarea').each(function() {
    		if( $(this).val() == "" ) {
    			e.preventDefault();
    			$(this).addClass('input-error');
    		}
    		else {
    			$(this).removeClass('input-error');
    		}
    	});
    	
    });
    
    
});

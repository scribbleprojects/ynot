function csrfSafeMethod(method) {
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        }
    }
});

$(()=>{
        var remember = $.cookie('rememberme');
        if (remember == 'true') 
        {
            var email = $.cookie('email');
            var password = $.cookie('password');
            $('#id_username').val(email);
            $('#id_password').val(password);
            $('#rememberme').attr('checked',true);
        }
    
    $('#login_form').on('submit', (event)=>{
        if($('#rememberme').is(':checked')){
            var email = $('#id_username').val();
            var password = $('#id_password').val();

            $.cookie('email', email);
            $.cookie('password', password);
            $.cookie('rememberme', true);

        }
        else{
            $.removeCookie('email');
            $.removeCookie('password');
            $.removeCookie('rememberme');
        }
        
    });

    let showpass = true;
    $('#showpass').on('click',function(event){
    	event.preventDefault();
        showpass ?
            ($('#showpass i').addClass("fa-eye"),
            $('#showpass i').removeClass("fa-eye-slash"),
        	$('#id_password').attr('type','text'),
            showpass = false)
        :
        	($('#showpass i').addClass("fa-eye-slash"),
                $('#showpass i').removeClass("fa-eye"),
        	$('#id_password').attr('type','password'),
            showpass = true)

    });
    $('#id_password').keyup(function(e) {
            // Detect current character & shift key
            var character = e.keyCode ? e.keyCode : e.which;
            var sftKey = e.shiftKey ? e.shiftKey : ((character == 16) ? true : false);
            // Is caps lock on?
            isCapsLock = (((character >= 65 && character <= 90) && !sftKey) || ((character >= 97 && character <= 122) && sftKey));
            // Display warning and set css
            if (isCapsLock == true) {
                var parent = $(".detectCapslocks");
                parent.addClass('capslock');
                $('.capslockMessage').remove();
                parent.append('<div class="capslockMessage">' + parent.data('message') + '</div>');
            }
            else
            {
               $(".capslockMessage").remove();
            }
        });

});